<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => env('SES_REGION', 'us-east-1'),
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
        'webhook' => [
            'secret' => env('STRIPE_WEBHOOK_SECRET'),
            'tolerance' => env('STRIPE_WEBHOOK_TOLERANCE', 300),
        ],
    ],

    'facebook' => [
        'client_id' => '719420668418746',         // Your GitHub Client ID
        'client_secret' => '15e79e8c4067a6eaa3b2156d698e4d10', // Your GitHub Client Secret
        'redirect' => 'http://localhost/demo/login/facebook/callback',
    ],

    'google' => [
        'client_id' => '1001846429393-hpvefk7bf0c39dcqgp6v1rq3dq84l86l.apps.googleusercontent.com',         // Your Google Client ID
        'client_secret' => 'zSMw-rUrAHDnlCPT8a1FmHYV', // Your Google Client Secret
        'redirect' => 'http://localhost/demo/login/google/callback',
    ],

];
