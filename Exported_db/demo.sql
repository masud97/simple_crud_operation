-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 02, 2019 at 07:19 AM
-- Server version: 10.1.35-MariaDB
-- PHP Version: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_09_24_065303_create_tbl_profiles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_audit_log`
--

CREATE TABLE `tbl_audit_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_audit_log`
--

INSERT INTO `tbl_audit_log` (`id`, `ip`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-22 23:49:18', '2018-10-22 23:49:18'),
(2, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-22 23:49:33', '2018-10-22 23:49:33'),
(3, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-22 23:50:51', '2018-10-22 23:50:51'),
(4, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 00:12:38', '2018-10-23 00:12:38'),
(5, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 03:09:44', '2018-10-23 03:09:44'),
(6, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 03:11:03', '2018-10-23 03:11:03'),
(7, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 03:40:27', '2018-10-23 03:40:27'),
(8, '::1', 'Admin login failed.', 'error', '2018-10-23 03:40:37', '2018-10-23 03:40:37'),
(9, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 03:40:48', '2018-10-23 03:40:48'),
(10, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 03:42:16', '2018-10-23 03:42:16'),
(11, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 03:47:24', '2018-10-23 03:47:24'),
(12, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 04:05:30', '2018-10-23 04:05:30'),
(13, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 04:09:10', '2018-10-23 04:09:10'),
(14, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 22:53:46', '2018-10-23 22:53:46'),
(15, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-23 23:27:40', '2018-10-23 23:27:40'),
(16, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 00:27:11', '2018-10-24 00:27:11'),
(17, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 00:40:00', '2018-10-24 00:40:00'),
(18, '::1', 'Admin login failed.', 'error', '2018-10-24 00:45:42', '2018-10-24 00:45:42'),
(19, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 00:45:50', '2018-10-24 00:45:50'),
(20, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 00:54:33', '2018-10-24 00:54:33'),
(21, '::1', 'Admin login failed.', 'error', '2018-10-24 01:01:35', '2018-10-24 01:01:35'),
(22, '::1', 'Admin login failed.', 'error', '2018-10-24 01:01:43', '2018-10-24 01:01:43'),
(23, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 01:01:50', '2018-10-24 01:01:50'),
(24, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 03:02:02', '2018-10-24 03:02:02'),
(25, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 03:02:47', '2018-10-24 03:02:47'),
(26, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 03:04:17', '2018-10-24 03:04:17'),
(27, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 03:05:24', '2018-10-24 03:05:24'),
(28, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 03:08:55', '2018-10-24 03:08:55'),
(29, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 22:46:21', '2018-10-24 22:46:21'),
(30, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 22:55:12', '2018-10-24 22:55:12'),
(31, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-24 23:13:46', '2018-10-24 23:13:46'),
(32, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-25 04:25:07', '2018-10-25 04:25:07'),
(33, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:15:12', '2018-10-27 22:15:12'),
(34, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:22:13', '2018-10-27 22:22:13'),
(35, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:23:38', '2018-10-27 22:23:38'),
(36, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:24:16', '2018-10-27 22:24:16'),
(37, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:26:28', '2018-10-27 22:26:28'),
(38, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:29:05', '2018-10-27 22:29:05'),
(39, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:30:12', '2018-10-27 22:30:12'),
(40, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:30:19', '2018-10-27 22:30:19'),
(41, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:31:28', '2018-10-27 22:31:28'),
(42, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:37:14', '2018-10-27 22:37:14'),
(43, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:37:49', '2018-10-27 22:37:49'),
(44, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-27 22:39:00', '2018-10-27 22:39:00'),
(45, '::1', 'Admin has been successfully logged in by Facebook.', 'success', '2018-10-28 03:31:37', '2018-10-28 03:31:37'),
(46, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-28 03:50:29', '2018-10-28 03:50:29'),
(47, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-28 04:10:16', '2018-10-28 04:10:16'),
(48, '::1', 'Admin has been successfully logged in.', 'success', '2018-10-28 04:11:17', '2018-10-28 04:11:17'),
(49, '::1', 'Admin has been successfully logged in by Facebook.', 'success', '2018-10-28 05:34:39', '2018-10-28 05:34:39'),
(50, '::1', 'Admin has been successfully logged in by Facebook.', 'success', '2018-10-28 05:38:32', '2018-10-28 05:38:32'),
(51, '::1', 'Admin has been successfully logged in by Facebook.', 'success', '2018-10-28 05:49:02', '2018-10-28 05:49:02'),
(52, '::1', 'Admin has been successfully logged in.', 'success', '2018-11-06 22:26:54', '2018-11-06 22:26:54'),
(53, '::1', 'Admin has been successfully logged in by Facebook.', 'success', '2018-11-06 22:30:52', '2018-11-06 22:30:52'),
(54, '::1', 'Admin has been successfully logged in.', 'success', '2018-11-17 00:41:41', '2018-11-17 00:41:41'),
(55, '::1', 'Admin has been successfully logged in.', 'success', '2018-11-17 00:48:36', '2018-11-17 00:48:36'),
(56, '::1', 'Admin has been successfully logged in.', 'success', '2018-11-20 00:54:01', '2018-11-20 00:54:01'),
(57, '::1', 'Admin has been successfully logged in.', 'success', '2018-11-27 22:27:23', '2018-11-27 22:27:23'),
(58, '::1', 'Admin has been successfully logged in.', 'success', '2018-11-28 01:41:35', '2018-11-28 01:41:35'),
(59, '::1', 'Admin has been successfully logged in.', 'success', '2018-12-11 22:53:07', '2018-12-11 22:53:07'),
(60, '::1', 'Admin has been successfully logged in.', 'success', '2019-04-01 23:14:23', '2019-04-01 23:14:23');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profiles`
--

CREATE TABLE `tbl_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tbl_profiles`
--

INSERT INTO `tbl_profiles` (`id`, `user_id`, `phone`, `gender`, `father_name`, `mother_name`, `dob`, `image`, `created_at`, `updated_at`, `status`) VALUES
(1, 1, '01841218335', 'male', 'Masud Rana', 'Sabina Khanom', '2018-10-01', '1490594887.jpg', '2018-10-22 18:00:00', '2018-11-16 18:00:00', 1),
(2, 2, '01710457845', NULL, 'Masud Rana', 'Sabina', '2018-10-02', '1002.jpg', '2018-10-22 18:00:00', '2018-10-22 18:00:00', 0),
(7, 7, '01721592088', 'male', NULL, NULL, '1988-02-20', 'demo.jpg', '2018-10-27 18:00:00', NULL, 1),
(8, 8, NULL, 'male', NULL, NULL, '1988-02-20', 'demo.jpg', '2018-11-06 18:00:00', NULL, 1),
(9, 9, '01814921385', NULL, 'Father Name', 'Mother Name', '2019-04-06', '1009.JPG', '2019-04-01 18:00:00', '2019-04-01 18:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Tasmid Aheel', 'admin@gmail.com', NULL, '$2y$10$tF5v.l0HkF72.eT6eTsiROdMB0iTJ/a3CFiSgdjSyv2LveQ0lsBbW', NULL, '2018-10-21 18:00:00', '2018-11-16 18:00:00', 1),
(2, 'New User', 'new@gmail.com', NULL, '$2y$10$xeJRMK1XqJGruux0DFaO1.rEvqlrCh.4ckpQR8W46MmFrAVHmK1o6', NULL, '2018-10-23 04:17:32', '2018-10-22 18:00:00', 0),
(7, 'Masud Rana', 'masudrana.iubat001@gmail.com', NULL, '$2y$10$8PhoBmQ7b8OayUAIPf9Pp.1ulKZCRc4rlwUBYONQ2GMMz../WV102', NULL, '2018-10-28 05:49:02', '2019-04-01 18:00:00', 0),
(8, 'Masud Rana', 'masudrana.iubat@gmail.com', NULL, '$2y$10$1NqHDIvG5Q2mg3S7CVk2F.9K.XznX/Cf3mIgBPwzjmfxB9rdvaf6K', NULL, '2018-11-06 22:30:52', '2018-11-06 22:30:52', 1),
(9, 'People', 'people@example.com', NULL, '$2y$10$SDxmEENYipnLOqllBPKN/.FdGgYJOXPKNw/vYF2D.Yh33qgk41kiq', NULL, '2019-04-01 23:16:00', '2019-04-01 18:00:00', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `tbl_audit_log`
--
ALTER TABLE `tbl_audit_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_profiles`
--
ALTER TABLE `tbl_profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbl_profiles_user_id_index` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_audit_log`
--
ALTER TABLE `tbl_audit_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `tbl_profiles`
--
ALTER TABLE `tbl_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_profiles`
--
ALTER TABLE `tbl_profiles`
  ADD CONSTRAINT `tbl_profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
