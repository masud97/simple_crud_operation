<?php

use Illuminate\Database\Seeder;
use App\Model\User;
use App\Model\Profile;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Profile::truncate();

        factory(User::class)->create();
        factory(Profile::class)->create();
    }
}
