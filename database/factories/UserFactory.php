<?php

use Faker\Generator as Faker;
use App\Model\User;
use App\Model\Profile;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => 'Admin User',
        'email' => 'admin@gmail.com',
        'email_verified_at' => now(),
        'password' => '$2y$10$tF5v.l0HkF72.eT6eTsiROdMB0iTJ/a3CFiSgdjSyv2LveQ0lsBbW',
        'remember_token' => str_random(10),
    ];
});


$factory->define(Profile::class, function (Faker $faker) {
    return [
        'user_id' => '1',
        'phone' => '01721592088',
        'gender' => 'male',
        'father_name' => 'Admin Father',
        'mother_name' => 'Admin Mother',
        'dob' => '2018-02-18',
        'image' => '1002.jpg',
    ];
});
