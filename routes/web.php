<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::group(['middleware' => ['loginCheck']], function(){
    /* Route for Admin Login */
    Route::get('/admin', 'Admin\AdminLoginController@index');
    Route::post('/check-login','Admin\AdminLoginController@check_login');

    /* login with facebook */
    Route::get('login/facebook', 'Admin\AdminLoginController@redirectToProvider');
    Route::get('login/facebook/callback', 'Admin\AdminLoginController@handleProviderCallback');

    /* login with google */
    Route::get('login/google', 'Admin\AdminLoginController@redirectToProviderGoogle');
    Route::get('login/google/callback', 'Admin\AdminLoginController@handleProviderCallbackGoogle');
});

Route::group(['middleware' => ['AdminCheck']], function(){    
    Route::get('/logout','Admin\AdminLoginController@logout');
    Route::get('/dashboard', 'Admin\AdminLoginController@admin_dashboard');
    /* Route for Admin Login */


    /* Route for Admin Profile */    
    Route::get('/new-admin', 'Admin\AdminController@create');
    Route::get('/view-admin', 'Admin\AdminController@index');
    Route::post('/store-admin', 'Admin\AdminController@store');
    Route::get('/view-details/{id}', 'Admin\AdminController@show');
    Route::get('/update-admin/{id}', 'Admin\AdminController@edit');
    Route::post('/store-update-info/{id}', 'Admin\AdminController@update');

    //Route::get('/admin-image-id/{id}', 'Admin\AdminController@update_image_pdate');
    Route::post('/change-admin-image/{id}','Admin\AdminController@change_admin_image');
    Route::post('/change-admin-password/{id}','Admin\AdminController@change_admin_password');
    //Route::get('/change-admin-password','Admin\AdminController@change_admin_password');


    Route::post('/upload-admin-image','Admin\AdminController@upload_admin_image');
    Route::get('/delete-admin/{id}','Admin\AdminController@destroy');
    /* Route for Admin Profile */

    
});
