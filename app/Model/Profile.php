<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\User;

class Profile extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id',
        'user_id',
        'phone',
        'gender',
        'father_name', 
        'mother_name', 
        'dob', 
        'image', 
        'created_at',
        'updated_at',
        'status'
    ];

    protected $table = 'tbl_profiles';

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
