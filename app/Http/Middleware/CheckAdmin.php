<?php

namespace App\Http\Middleware;

use Closure;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->session()->get('is_login'))
        {
            return redirect('/admin')->withErrors('Please Login');          
        }
        return $next($request);  
        //return redirect()->back()->withErrors('Login First Please');
        //return redirect()->action('App\Http\Controllers\Admin\AdminLoginController@index');
    }
}
