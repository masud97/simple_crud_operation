<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Socialite;
use App\Model\User;
use App\Model\Profile;

class AdminLoginController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('pages.admin.login.login',['title'=>'Login']);
    }

    public function admin_dashboard(){
        return view('pages.admin.dashboard.dashboard',['title' => "Dashboard"]);
    }



    public function check_login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required | email',
            'password' => 'required',
                ], [
            'email.required' => 'Email is required field.',
            'email.email' => 'It should be a valid email address.',
            'password.required' => 'Password field is required.'
        ]);

        $id = $request->input('email');
        $password = $request->input('password');
        
        $user = User::select('id','name','email','password')->where('email',$id)->with('profiles')->first();
        
        if(!is_null($user)){
            $check_password = ck_password($password, $user->password);
            if($check_password){
                $data = array(
                    'user_id' => $user->id,
                    'name' => $user->name,
                    'email' => $user->email,
                    'user_image' => $user->profiles->image,
                    'is_login' => TRUE
                );
                
                $request->session()->put($data);
                addLog($user->id, 'Admin has been successfully logged in.', 'success');
                return view('pages.admin.dashboard.dashboard',['title' => "Dashboard"]);
            }else{
                addLog($user->id, 'Admin login failed.', 'error');
                return redirect()->back()->withErrors(['Incorrect User Id or Password']);
            }
        }else{
            return redirect()->back()->withErrors(['Incorrect User Id or Password']);
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

     /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->fields([
            'last_name', 'first_name', 'name', 'email', 'gender', 'birthday'
        ])->scopes([
            'email', 'user_gender', 'user_birthday'
        ])->redirect();
    }


    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback(Request $request)
    {
        $facebook_user = Socialite::driver('facebook')->fields([
         'first_name', 'last_name', 'name', 'email', 'gender', 'birthday'
        ])->user();
        
        $checkOldUser = User::select('id','name','email','password')->where('email',$facebook_user->email)->with('profiles')->first();
        
        if(!is_null($checkOldUser)){
            echo "Email already exists.";
        }else{
            $password = '123456';
            $hash_pass = password_hash($password, PASSWORD_DEFAULT);

            $userData = array(
                'name' => $facebook_user->name,
                'email' => $facebook_user->email,
                'password' => $hash_pass,
                'created_at' => date('y-m-d')
            );

            if (User::create($userData)){
                $userId = User::select('id')->orderBy('id', 'DESC')->first();
                $profileData = array(
                    'user_id' => $userId->id,
                    'gender' => $facebook_user->user['gender'],
                    'dob' => date("Y-m-d", strtotime($facebook_user->user['birthday'])),
                    'image' => 'demo.jpg',
                    'created_at' => date('y-m-d')
                );
                if (Profile::create($profileData)) {
                    $data = array(
                        'user_id' => $userId->id,
                        'name' => $facebook_user->name,
                        'email' => $facebook_user->email,
                        'is_login' => TRUE
                    );
                    
                    $request->session()->put($data);
                    addLog($userId->id, 'Admin has been successfully logged in by Facebook.', 'success');
                    return view('pages.admin.dashboard.dashboard',['title' => "Dashboard"]);
                }else{
                    Session::flash('errmessage', 'Facebook login failed.');
                    return redirect()->back();
                }            
            }else{
                Session::flash('errmessage', 'Facebook login failed.');
                return redirect()->back();
            }
        }        
    }


    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallbackGoogle(Request $request)
    {
        $google_user = Socialite::driver('google')->user();

        return $google_user->name;

        die();
        
        $checkOldUser = User::select('id','name','email','password')->where('email',$facebook_user->email)->with('profiles')->first();
        
        if(!is_null($checkOldUser)){
            echo "Email already exists.";
        }else{
            $password = '123456';
            $hash_pass = password_hash($password, PASSWORD_DEFAULT);

            $userData = array(
                'name' => $facebook_user->name,
                'email' => $facebook_user->email,
                'password' => $hash_pass,
                'created_at' => date('y-m-d')
            );

            if (User::create($userData)){
                $userId = User::select('id')->orderBy('id', 'DESC')->first();
                $profileData = array(
                    'user_id' => $userId->id,
                    'gender' => $facebook_user->user['gender'],
                    'dob' => date("Y-m-d", strtotime($facebook_user->user['birthday'])),
                    'image' => 'demo.jpg',
                    'created_at' => date('y-m-d')
                );
                if (Profile::create($profileData)) {
                    $data = array(
                        'user_id' => $userId->id,
                        'name' => $facebook_user->name,
                        'email' => $facebook_user->email,
                        'is_login' => TRUE
                    );
                    
                    $request->session()->put($data);
                    addLog($userId->id, 'Admin has been successfully logged in by Facebook.', 'success');
                    return view('pages.admin.dashboard.dashboard',['title' => "Dashboard"]);
                }else{
                    Session::flash('errmessage', 'Facebook login failed.');
                    return redirect()->back();
                }            
            }else{
                Session::flash('errmessage', 'Facebook login failed.');
                return redirect()->back();
            }
        }        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request){
        $request->session()->flush();
        return redirect('/admin');
    }
}
