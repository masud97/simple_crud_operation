<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use App\Model\User;
use App\Model\Profile;
use Session;
use DB;
use File;
use App\Library\PrivilegeCheckLib as PrivilegeCheckLib;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $adminList = User::select('id','name','email')->where('status', 1)->with('profiles')->get();
        return view('pages.admin.profile.view-admin', ['adminList' => $adminList, 'title' => 'View Admin']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {               
        return view('pages.admin.profile.create-admin', ['title' => 'Create Admin']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required | email | unique:users',
            'password' => 'required',
            'repassword' => 'required | same:password',
            'phone' => 'required|unique:tbl_profiles',
            'father_name' => 'required',
            'mother_name' => 'required',
            'dob' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
                ], [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required field.',
            'email.email' => 'It should be a valid email address.',
            'email.unique' => 'This email address is already exists.',
            'password.required' => 'Password field is required.',
            'repassword.required' => 'confirm password field is required.',
            'repassword.same' => 'Both password fields are not match.',
            'phone.required' => 'Phone field should not be empty!',
            'phone.unique' => 'This phone number is already exists.',
            'father_name.required' => 'Father name field should not be empty!',
            'mother_name.required' => 'Mother name field should not be empty!',
            'dob.required' => 'Date of birth field should not be empty.',
            'image.image' => 'File should be an Image.',
            'image.mimes' => 'File should be jpeg, png, jpg, gif, svg.',
            'image.max' => 'Maximum Image size is 2048'
        ]);

        
        
		$lastid = User::select('id')->orderBy('id', 'DESC')->first();
        $imageName = 1000 + $lastid->id + 1 . '.' . $request->image->getClientOriginalExtension();
        $request->image->move(('assets/admin/images/admin'), $imageName);

        $password = $request->input('password');

        $hash_pass = password_hash($password, PASSWORD_DEFAULT);
        //  Create Password for User 

        $userData = array(
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => $hash_pass,
            'created_at' => date('y-m-d')
        );

        if (User::create($userData)) {
            $userId = User::select('id')->orderBy('id', 'DESC')->first();
            $profileData = array(
                'user_id' => $userId->id,
                'phone' => $request->input('phone'),
                'father_name' => $request->input('father_name'),
                'mother_name' => $request->input('mother_name'),
                'dob' => $request->input('dob'),
                'image' => $imageName,
                'created_at' => date('y-m-d')
            );
            if (Profile::create($profileData)) {
                Session::flash('message', 'New Admin created.');
                return Redirect::to('/view-details/'.$userId->id);
            }else{
                Session::flash('errmessage', 'New Admin creation failed.');
                return redirect()->back();
            }            
        }else{
            Session::flash('errmessage', 'New Admin creation failed.');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $adminInfo = User::select('id','name','email')->where('status', 1)->with('profiles')->find($id);
        return view('pages.admin.profile.view-details',['adminInfo' => $adminInfo, 'title' => 'View Details']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id) {
        $adminInfo = User::select('id','name','email')->where('status', 1)->with('profiles')->find($id);
        $data = array(
            'admin_id' => $adminInfo->id
        );
        $request->session()->put($data);
        return view('pages.admin.profile.update-admin',['adminInfo' => $adminInfo, 'title' => 'Update Admin']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required | email',
            'phone' => 'required',
            'father_name' => 'required',
            'mother_name' => 'required',
            'dob' => 'required'
                ], [
            'name.required' => 'Name is required.',
            'email.required' => 'Email is required field.',
            'email.email' => 'It should be a valid email address.',
            'phone.required' => 'Phone is required field.',
            'father_name.required' => 'Father Name is required field.',
            'mother_name.required' => 'Mother Name is required field.',
            'dob.required' => 'Date of Birth is required field.'
        ]);

        

        $userInfo = array(
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'updated_at' => date('y-m-d')
        );

        
        $userInfoUpdate = User::where('id', $id)->update($userInfo);        
            $profileInfo = array(
                'phone' => $request->input('phone'),
                'father_name' => $request->input('father_name'),
                'mother_name' => $request->input('mother_name'),
                'dob' => $request->input('dob'),
                'updated_at' => date('y-m-d')
            );

        $profileInfoUpdate = Profile::where('user_id', $id)->update($profileInfo);
           
            if(!is_null($userInfoUpdate) || !is_null($profileInfoUpdate)){
                Session::flash('message', 'Information successfully updated.');
                return Redirect::to('/view-details/'.$id);
            }else{
                Session::flash('errmessage', 'Profile Information update failed.');
                return redirect()->back();
            }
    }


    public function update_image_pdate($id){
        $adminImage = Profile::select('image')->where('user_id',$id)->first();
        return response()->json(['adminImage' => $adminImage], 200);
    }

    public function change_admin_image(Request $request, $id){
        // Upload Owner Image
            $userImage = Profile::select('image')->where('user_id',$id)->first();

            if($userImage->image == 'demo.jpg'){
                $lastid = User::select('id')->orderBy('id', 'DESC')->first();
                $newImageName = 2000 + $lastid->id + 1 . '.' . $request->new_profile_image->getClientOriginalExtension();
            }else{
                $imageName = explode(".",$userImage->image);            
                $newImageName = $imageName[0]. '.' . $request->new_profile_image->getClientOriginalExtension();                
            }


            
            if (Input::hasFile('new_profile_image')) {
                $file = $request->file('new_profile_image');
                
                $fileExtent = $file->getClientOriginalExtension(); //file extension
                $extent = array('jpeg', 'jpg', 'bmp', 'png', 'gif', 'svg');
                
                if (in_array($fileExtent, $extent)) {
                    $destinationPath = 'assets/admin/images/admin';
                    
                    $file->move($destinationPath, $newImageName); //here we rename with the valid file extension
                    return redirect()->back();
                } else {
                    return redirect()->back()->withErrors('File in not an image');
                }
            } else {
                $upload_image = NULL;
            }
        // Upload Owner Image
        Profile::where('user_id', $id)->update(['updated_at' => date('Y-m-d'), 'image' => $newImageName]);
        Session::flash('message', 'Profile Image has changed.');
        return redirect()->back();
    }



    public function change_admin_password(Request $request, $id){       
        $this->validate($request, [
            'new_password' => 'required',
            'conf_new_password' => 'required | same:new_password'
                ], [
            'new_password.required' => 'New Password field is required.',
            'conf_new_password.required' => 'confirm password field is required.',
            'conf_new_password.same' => 'Both password fields are not match.',
        ]);

        $password = $request->input('new_password');
        $hash_pass = password_hash($password, PASSWORD_DEFAULT);
        //  Create Password for User 
        
        //$id = Session::get('user_id');

        $updateResult = User::where('id', $id)->update(['updated_at' => date('Y-m-d'), 'password' => $hash_pass]);
        if($updateResult){
            Session::flash('changepasswordmessage', 'Delete Successful.');
            return redirect()->back();
        }else{
            Session::flash('changePasswordFailed', 'Delete Failed. Try again');
            return redirect()->back();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {        
        User::where('id', $id)->update(['updated_at' => date('Y-m-d'), 'status' => 0]);
        Session::flash('message', 'Delete Successful.');
        return redirect()->back();
    }
	
	public function upload_admin_image(Request $request){
		
		$adminId = Session::get('user_id');
		
		// Upload Owner Image
            $imageName = 1000 + $adminId  . '.' . $request->image->getClientOriginalExtension();
                if (Input::hasFile('image')) {
                    $file = $request->file('image');
                    
                    $fileExtent = $file->getClientOriginalExtension(); //file extension
                    $extent = array('jpeg', 'jpg', 'bmp', 'png', 'gif', 'svg');
                    
                    if (in_array($fileExtent, $extent)) {
                        $destinationPath = 'assets/images';
                        
                        $file->move($destinationPath, $imageName); //here we rename with the valid file extension
						return redirect('/admin-personal-profile/'.$adminId);
                    } else {
                        return redirect()->back()->withErrors('File in not an image');
                    }
                } else {
                    $upload_image = NULL;
                }
		// Upload Owner Image

	}

}
