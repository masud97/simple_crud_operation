<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditLog extends Model
{
    protected  $fillable = [
        'ip', 'description','status',
    ];

    protected $table = 'tbl_audit_log';
}
