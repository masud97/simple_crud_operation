<?php

namespace App\Library;
use DB;
use App\AuditLog;



class AuditTrailLib{
    
    public static function addTrail($access_by,$description,$status)
    {
       
        AuditLog::create(array(
            'ip'=> \Request::ip(),
            'access_by'=>$access_by,
            'description'=>$description,
            'status'=>$status
        ));
    }
    
}