<?php

namespace App\Library;

use Illuminate\Support\Facades\Route;
use App\Model\AdminModel as AdminModel;

class PrivilegeCheckLib {
    public static function hasPrivilege($id) {
        $currentUrl = Route::getFacadeRoot()->current()->uri();

		if(session('is_login')){
			$user = AdminModel::select('previlige')
                ->where(array('user_status' => 1, 'id' => session('user_id')))
                ->first();
			$privlist = NULL;	
			if(!is_null($user) && count($user) >0 ){
				$privlist = explode(",",$user->previlige);
				if(in_array("$id",$privlist)){
					//printIt('muri', FALSE, TRUE);
					return TRUE;
				}else{
					//printIt('khaa', FALSE, TRUE);
					return FALSE;
				}
			}else{
				return redirect('/')->withErrors('please login first');
			}				
		}else{
			return redirect('/')->withErrors('please login first');
		}
    }

    /* This function return true or false. If privileged exists it return true else false */

    public static function checkPrivilege($privilegeId, $id) {
        return LoginModel::whereRaw('FIND_IN_SET(' . $privilegeId . ',previlige)')->where(array('id' => $id, 'user_status' => 1))->exists();
    }

    public static function hasActive($id) {	
        return LoginModel::where(array('id' => $id, 'user_status' => 1))->exists();
    }

}
