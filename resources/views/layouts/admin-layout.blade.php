<!DOCTYPE HTML>
<html lang="en-US">
<head>
	@include('pertials.admin.head')
</head>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">
		<!-- Include header -->
			@include('pertials.admin.header')
			<!-- Include header -->
			
			<!-- Include sidebar -->
			@include('pertials.admin.sidebar')
			<!-- Include sidebar -->
	
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			@yield('content')
		</div>
		<!-- Content Wrapper. Contains page content -->
		<!-- Include footer -->
			@include('pertials.admin.footer')
			<!-- Include footer -->
			
			<!-- Include control sidebar -->
			
			<!-- Include control sidebar -->
	</div>
		@if(isset($customJs))
        @include("pertials.admin.js.$customJs")
        @else
			@include('pertials.admin.js.default-js')
		@endif
</body>
</html>