@extends('layouts.admin-layout')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-graduation-cap"></i>Admin</a></li>
        <li class="active">New Admin</li>
    </ol>
</section>
<section class="content" style="margin-top:25px;">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">New Admin</h3>
                    @if (Session::has('message'))
                    <div class="alert alert-info">{{ Session::get('message') }}</div>
                    @endif
                    @if(Session::has('errmessage'))
                    <div class="alert alert-danger">{{ Session::get('errmessage') }}</div>
                    @endif
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!-- form start -->
                    <form class="form-horizontal" action="{{url('/store-admin')}}" method='POST' enctype="multipart/form-data">
                        <div class="box-body box-profile">
                            <div class="row">
								<div class="col-md-6">
									<input type="hidden" name="_token" value="{{csrf_token()}}">

									<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
										<label for="inputEmail3" class="col-sm-3 control-label">Name <span class="text-danger"> * </span></label>
										<div class="col-sm-9"> 
											<input type="text" name="name" class="form-control" id="inputPassword3" value="{{old('name')}}" />
											<span class="text-danger">{{ $errors->first('name') }}</span>
										</div>
									</div>
									<div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
										<label for="inputEmail3" class="col-sm-3 control-label">Phone <span class="text-danger"> * </span></label>
										<div class="col-sm-9"> 
											<input type="text" name="phone" class="form-control" id="inputPassword3" value="{{old('phone')}}" />
											<span class="text-danger">{{ $errors->first('phone') }}</span>
										</div>
									</div>
									<div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
										<label for="inputPassword3" class="col-sm-3 control-label">Password <span class="text-danger"> * </span></label>
										<div class="col-sm-9">
											<input type="password" name="password" class="form-control" id="inputPassword3" />
											<span class="text-danger">{{ $errors->first('password') }}</span>
										</div>
									</div>
									<div class="form-group {{ $errors->has('repassword') ? 'has-error' : '' }}">
										<label for="inputPassword3" class="col-sm-3 control-label">Conf Password <span class="text-danger"> * </span></label>
										<div class="col-sm-9">
											<input type="password" name="repassword" class="form-control" id="inputPassword3" />
											<span class="text-danger">{{ $errors->first('repassword') }}</span>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-4 col-sm-offset-4">
											<img id="output" height="150px" width="150px"/>
										</div>
										<div class="col-sm-4"></div>
									</div>
									<div class="form-group {{ $errors->has('image') ? 'has-error' : '' }}">
										<label for="inputPassword3" class="col-sm-3 control-label">Image <span class="text-danger"> * </span></label>
										<div class="col-sm-9">
											<input type="file" name="image" class="" id="insertImage" accept="image/*" onchange="loadFile(event)" />
											<span class="text-danger">{{ $errors->first('image') }}</span>
										</div>
									</div>
								</div>
								<div class="col-md-6">
                                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
										<label for="inputPassword3" class="col-sm-3 control-label">Email <span class="text-danger"> * </span></label>
										<div class="col-sm-9">
											<input type="email" name="email" class="form-control" id="inputPassword3" value="{{old('email')}}" />
											<span class="text-danger">{{ $errors->first('email') }}</span>
										</div>
									</div>
									<div class="form-group {{ $errors->has('father_name') ? 'has-error' : '' }}">
										<label for="inputEmail3" class="col-sm-3 control-label">Father Name <span class="text-danger"> * </span></label>
										<div class="col-sm-9"> 
											<input type="text" name="father_name" class="form-control" id="inputPassword3" value="{{old('father_name')}}" />
											<span class="text-danger">{{ $errors->first('father_name') }}</span>
										</div>
									</div>
									<div class="form-group {{ $errors->has('mother_name') ? 'has-error' : '' }}">
										<label for="inputEmail3" class="col-sm-3 control-label">Mother Name <span class="text-danger"> * </span></label>
										<div class="col-sm-9"> 
											<input type="text" name="mother_name" class="form-control" id="inputPassword3" value="{{old('mother_name')}}" />
											<span class="text-danger">{{ $errors->first('mother_name') }}</span>
										</div>
									</div>
									<div class="form-group {{ $errors->has('dob') ? 'has-error' : '' }} date">
										<label for="inputEmail3" class="col-sm-3 control-label">Date of Birth <span class="text-danger"> * </span></label>
										<div class="col-sm-9"> 
											<input type="text" name="dob" class="form-control" id="datepicker" data-date-format='yyyy-mm-dd' value="{{old('dob')}}"/>
											<span class="text-danger">{{ $errors->first('dob') }}</span>
										</div>
									</div>
								</div>
							</div>
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">
                            <button type="submit" name="submit" class="btn btn-info pull-right">Create</button>
                        </div>
                        <!-- /.box-footer -->
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection