@extends('layouts.admin-layout')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-graduation-cap"></i>Admin</a></li>
        <li class="active">Admin List</li>
    </ol>
</section>
<section class="content" style="margin-top:25px;">
    <div class="row">
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">View Admin</h3>
                @if (Session::has('message'))
                <div class="alert alert-info">{{ Session::get('message') }}</div>
                @endif
                @if(Session::has('errmessage'))
                <div class="alert alert-danger">{{ Session::get('errmessage') }}</div>
                @endif
            </div>
            <!-- /.box-header -->
            <!-- table start -->
            <?php $i = 1; ?>
            <table class="table table-bordered" style="margin-left:20px;">
                <thead>
                    <tr>
                        <th>SL.</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($adminList as $list)
                    <tr>
                        <td><?php echo $i++; ?></td>
                        <td>{{$list->name}}</td>
                        <td>{{$list->email}}</td>
                        <td>
                            @if(!is_null($list->profiles->image))
                                <img src="{{url('assets/admin/images/admin')}}/{{$list->profiles->image}}" style='height: 50px; width:50px'>
                            @else
                                <img src="{{url('assets/admin/images/admin/demo.jpg')}}" style='height: 50px; width:50px'>
                            @endif
                        </td>
                        <td  style="min-width:100px;">
                                <a href="{{url('/view-details')}}/{{$list->id}}" title="View Details" data-placement="top" data-toggle="tooltip" data-original-title="View Details" class="btn btn-info tooltips"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                <a href="{{url('/update-admin')}}/{{$list->id}}" title="Update" data-placement="top" data-toggle="tooltip" data-original-title="Update Information" class="btn btn-info tooltips"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                <a href="{{url('/delete-admin')}}/{{$list->id}}" title="Delete Admin" data-placement="top" data-toggle="tooltip" data-original-title="Delete Admin" class="btn btn-danger tooltips confirm"><i class="fa fa-times" aria-hidden="true"></i></a>
                            </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <!-- table start -->
        </div>
    </div>
    </div>
</section>
@endsection