@extends('layouts.admin-layout')

@section('content')
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-graduation-cap"></i>Profile</a></li>
        <li class="active">My Profile</li>
    </ol>
</section>
<section class="content" style="margin-top:25px;">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xs-offset-0 col-sm-offset-0 col-md-offset-2 col-lg-offset-2 toppad" >


            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">{{Session::get('user_name')}}</h3>
                    <span data-toggle="modal" data-target="#infoUpdateModal" class="pull-right" style="margin-top: -23px;">     
                        <a data-original-title="Update personal info" data-toggle="tooltip" type="button" class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-edit"></i></a>
                    </span>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <span data-toggle="modal" data-target="#imgchangeModal">
                            <div class="col-md-3 col-lg-3 " align="center"> <a data-original-title="Update porfile image" data-toggle="tooltip" href="#imgchangeModal"><img alt="User Pic" src="{{url('assets/images/'.Session::get('user_image'))}}" class="img-circle img-responsive"></a></div>
                        </span>
                        <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                          <dl>
                            <dt>DEPARTMENT:</dt>
                            <dd>Administrator</dd>
                            <dt>HIRE DATE</dt>
                            <dd>11/12/2013</dd>
                            <dt>DATE OF BIRTH</dt>
                               <dd>11/12/2013</dd>
                            <dt>GENDER</dt>
                            <dd>Male</dd>
                          </dl>
                        </div>-->
                        <div class=" col-md-9 col-lg-9 "> 
                            <table class="table table-user-information">
                                <tbody>
                                    <tr>
                                        <td>Full Name:</td>
                                        <td>{{Session::get('user_name')}}</td>
                                    </tr>
                                    <tr>
                                        <td>Email Address:</td>
                                        <td>{{Session::get('user_email')}}</td>
                                    </tr>
                                    <tr>
                                        <td>Phone:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Date of Birth:</td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>Gender:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Home Address:</td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Blood Group:</td>
                                        <td></td>
                                    </tr>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- imgchangeModal -->
    <div class="modal fade" id="imgchangeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Change your profile Image</h4>
                </div>
                <form action="{{url('/upload-admin-image')}}" method="POST" class="form-horizontal demo-form" enctype="multipart/form-data">
                    <div class="modal-body">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
                        <img alt="User Pic" src="{{url('assets/images/'.Session::get('user_image'))}}" class="img-circle img-responsive" id="output" style="margin-left: 175px;" height="160px" width='160px'>
                        &nbsp;
                        &nbsp;

                        <input type="file" name="image" id="image" accept="image/*" onchange="loadFile(event)" style="margin-left: 175px;"/>

                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="submit" value="Update" class="btn btn-primary" style="color:white;"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- imgchangeModal -->
    <!-- infoUpdateModal -->
	<div class="modal fade" id="infoUpdateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Update your Personal info</h4>
                </div>
                <form action="{{url('/upload-admin-image')}}" method="POST" class="form-horizontal demo-form" enctype="multipart/form-data">
                    <div class="modal-body">
						<input type="hidden" name="_token" value="{{csrf_token()}}">
                        <table class="table table-user-information">
                                <tbody>
                                    <tr>
                                        <td>Full Name:</td>
                                        <td><input type="text" name="admin_name" class="form-control" value="{{Session::get('user_name')}}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Email Address:</td>
                                        <td><input type="text" name="admin_name" class="form-control" value="{{Session::get('user_email')}}"/></td>
                                    </tr>
                                    <tr>
                                        <td>Phone:</td>
                                        <td><input type="text" name="admin_name" class="form-control" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td>Date of Birth:</td>
                                        <td><input type="text" name="admin_name" class="form-control" value=""/></td>
                                    </tr>

                                    <tr>
                                    <tr>
                                        <td>Gender:</td>
                                        <td><input type="text" name="admin_name" class="form-control" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td>Home Address:</td>
                                        <td><input type="text" name="admin_name" class="form-control" value=""/></td>
                                    </tr>
                                    <tr>
                                        <td>Blood Group:</td>
                                        <td><input type="text" name="admin_name" class="form-control" value=""/></td>
                                    </tr>
                                    </tr>
                                </tbody>
                            </table>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" name="submit" value="Update" class="btn btn-primary" style="color:white;"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- infoUpdateModal -->
    @endsection