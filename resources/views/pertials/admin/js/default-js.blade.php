<!-- jQuery 2.2.3 -->
<script src="{{url('assets/admin/plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
$.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="{{url('assets/admin/bootstrap/js/bootstrap.min.js')}}"></script>
<!-- Morris.js charts -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
<!--<script src="{{url('assets/plugins/morris/morris.min.js')}}"></script>-->
<!-- Sparkline -->
<script src="{{url('assets/admin/plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{url('assets/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{url('assets/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{url('assets/admin/plugins/knob/jquery.knob.js')}}"></script>
<!-- daterangepicker -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{url('assets/admin/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- datepicker -->
<script src="{{url('assets/admin/plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<script src="{{url('assets/admin/plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<!-- FastClick -->
<script src="{{url('assets/admin/plugins/fastclick/fastclick.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{url('assets/admin/dist/js/app.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="{{url('assets/dist/js/pages/dashboard.js')}}"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="{{url('assets/dist/js/demo.js')}}"></script>

<script src="{{url('assets/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{url('assets/admin/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>

<!-- Confirmation message -->
<script src="{{url('assets/admin/js/jquery.confirm.min.js')}}"></script>

<!-- Datepeaker js -->
<script src="{{url('assets/admin/js/bootstrap-datetimepicker.js')}}"></script>
<script src="{{url('assets/admin/js/bootstrap-datetimepicker.fr.js')}}"></script>
<script src="{{url('assets/admin/js/common-scripts.js')}}"></script>


<!-- fullCalendar 2.2.5 -->
<script src="{{url('assets/admin/js/moment.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>
<script src="{{url('assets/admin/plugins/fullcalendar/fullcalendar.min.js')}}"></script>



<script>
    $(function () {
        $("#example1").DataTable();
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>

<script>
    $(function () {
        $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false
        });
    });
</script>

<script>
    $(".confirm").confirm({
        text: "Are you sure to disable this!",
        title: "Confirmation required",
    });
</script>






<script>
    $('.selected_course_assing').change(function () {
        var course = $('.selected_course_assing').val();
        var url = "{{url('/selected-course-assing')}}/" + course;
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            var $coursetosection = $('#coursetosection');
            $coursetosection.empty();
            $coursetosection.append('<option value="#">Select Section</option>');
            for (var i = 0; i < data.sectionreserve.length; i++) {
                $coursetosection.append('<option value=' + data.sectionreserve[i]['section'] + '>' + data.sectionreserve[i]['section'] + '</option>');
            }
        });

    });
</script>




<script>
    function myFunction() {
        console.log("Yes! you are here!");
        var adminid = "{{ Session::get('admin_id')}}";
        var url = "{{url('/admin-image-id')}}/" + adminid;
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            console.log(data.adminImage['image']);
            var image = "{{url('assets/admin/images/admin')}}/" + data.adminImage['image'];
            console.log(image);
            $('#output').html('<img src="' + image  + '" />');                   
        });
    }
</script>

<script>
    $('.secletedsection').change(function () {
        var section = $('.secletedsection').val();
        var course = $('.selected_course_assing').val();
        var concat = section + ',' + course;
        var url = "{{url('/selected-course-section')}}/" + concat;
        $.ajax({
            url: url,
            method: "GET",
        }).done(function (data) {
            var $selectday = $('#selectday');
            $selectday.empty();
            $selectday.append('<option value="#">Select Day</option>');
            for (var i = 0; i < data.alldays.length; i++) {
                $selectday.append('<option value=' + data.alldays[i] + '>' + data.alldays[i] + '</option>');
            }
        });

    });
</script>






<script>
    var loadFile = function (event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>


<script>
    var loadFile = function (event) {
        var output = document.getElementById('output');
        output.src = URL.createObjectURL(event.target.files[0]);
    };
</script>

<script>
    var loadFile = function (event) {
        var modaloutput = document.getElementById('modaloutput');
        modaloutput.src = URL.createObjectURL(event.target.files[0]);
    };
</script>




<!-- JQuery for calendar -->

<script>
      $(function () {

        /* initialize the external events
         -----------------------------------------------------------------*/
        function ini_events(ele) {
          ele.each(function () {

            // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
            // it doesn't need to have a start or end
            var eventObject = {
              title: $.trim($(this).text()) // use the element's text as the event title
            };

            // store the Event Object in the DOM element so we can get to it later
            $(this).data('eventObject', eventObject);

            // make the event draggable using jQuery UI
            $(this).draggable({
              zIndex: 1070,
              revert: true, // will cause the event to go back to its
              revertDuration: 0  //  original position after the drag
            });

          });
        }
        ini_events($('#external-events div.external-event'));

        /* initialize the calendar
         -----------------------------------------------------------------*/
        //Date for the calendar events (dummy data)
        var date = new Date();
        var d = date.getDate(),
                m = date.getMonth(),
                y = date.getFullYear();
        $('#calendar').fullCalendar({
          header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
          },
          buttonText: {
            today: 'today',
            month: 'month',
            week: 'week',
            day: 'day'
          },
          //Random default events
          events: [
            {
              title: 'All Day Event',
              start: new Date(y, m, 1),
              backgroundColor: "#f56954", //red
              borderColor: "#f56954" //red
            },
            {
              title: 'Long Event',
              start: new Date(y, m, d - 5),
              end: new Date(y, m, d - 2),
              backgroundColor: "#f39c12", //yellow
              borderColor: "#f39c12" //yellow
            },
            {
              title: 'Meeting',
              start: new Date(y, m, d, 10, 30),
              allDay: false,
              backgroundColor: "#0073b7", //Blue
              borderColor: "#0073b7" //Blue
            },
            {
              title: 'Lunch',
              start: new Date(y, m, d, 12, 0),
              end: new Date(y, m, d, 14, 0),
              allDay: false,
              backgroundColor: "#00c0ef", //Info (aqua)
              borderColor: "#00c0ef" //Info (aqua)
            },
            {
              title: 'Birthday Party',
              start: new Date(y, m, d + 1, 19, 0),
              end: new Date(y, m, d + 1, 22, 30),
              allDay: false,
              backgroundColor: "#00a65a", //Success (green)
              borderColor: "#00a65a" //Success (green)
            },
            {
              title: 'Click for Google',
              start: new Date(y, m, 28),
              end: new Date(y, m, 29),
              url: 'http://google.com/',
              backgroundColor: "#3c8dbc", //Primary (light-blue)
              borderColor: "#3c8dbc" //Primary (light-blue)
            }
          ],
          editable: true,
          droppable: true, // this allows things to be dropped onto the calendar !!!
          drop: function (date, allDay) { // this function is called when something is dropped

            // retrieve the dropped element's stored Event Object
            var originalEventObject = $(this).data('eventObject');

            // we need to copy it, so that multiple events don't have a reference to the same object
            var copiedEventObject = $.extend({}, originalEventObject);

            // assign it the date that was reported
            copiedEventObject.start = date;
            copiedEventObject.allDay = allDay;
            copiedEventObject.backgroundColor = $(this).css("background-color");
            copiedEventObject.borderColor = $(this).css("border-color");

            // render the event on the calendar
            // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
            $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

            // is the "remove after drop" checkbox checked?
            if ($('#drop-remove').is(':checked')) {
              // if so, remove the element from the "Draggable Events" list
              $(this).remove();
            }

          }
        });

        /* ADDING EVENTS */
        var currColor = "#3c8dbc"; //Red by default
        //Color chooser button
        var colorChooser = $("#color-chooser-btn");
        $("#color-chooser > li > a").click(function (e) {
          e.preventDefault();
          //Save color
          currColor = $(this).css("color");
          //Add color effect to button
          $('#add-new-event').css({"background-color": currColor, "border-color": currColor});
        });
        $("#add-new-event").click(function (e) {
          e.preventDefault();
          //Get value and make sure it is not null
          var val = $("#new-event").val();
          if (val.length == 0) {
            return;
          }

          //Create events
          var event = $("<div />");
          event.css({"background-color": currColor, "border-color": currColor, "color": "#fff"}).addClass("external-event");
          event.html(val);
          $('#external-events').prepend(event);

          //Add draggable funtionality
          ini_events(event);

          //Remove event from text input
          $("#new-event").val("");
        });
      });
    </script>	
	
    <script type="text/javascript">
        //Date picker
        $('#datepicker').datepicker({
            autoclose: true,
            dateFormat: 'yy-mm-dd'
        });
    </script>


    @if($errors->first('conf_new_password') || $errors->first('new_password'))
      <script>          
        $('#changePasswordModal').modal('show');
      </script>
    @endif

    @if (Session::has('changepasswordmessage'))
        <script>          
            $('#successModal').modal('show');
      </script>
    @endif



<!-- Start Script for Change Password Modal -->

<!-- End Script for Change Password Modal -->
<!-- JQuery for calendar -->