<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="text-center image">
                @if(Session::has('user_image'))
                    <img src="{{url('assets/admin/images/admin/'.Session::get('user_image'))}}" class="img-circle" alt="User Image">
                @else
                    <img src="{{url('assets/admin/images/admin/demo.jpg')}}" class="img-circle" alt="User Image">
                @endif    
            </div>
            <div class="pull-left info">
                <p style="margin-top:10px">
                    @if (Session::has('user_name'))
                    {{ Session::get('user_name') }}
                    @endif
                </p>
            </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
            @if(getCurrentUrl() === 'dashboard')
                <a href="{{url('/dashboard')}}" class="active">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            @else
                <a href="{{url('/dashboard')}}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
            @endif
            </li>
            <li class="treeview">
            @if(getCurrentUrl() === 'new-admin' or getCurrentUrl() === 'view-admin' or getCurrentUrl() === 'view-details/{id}' or getCurrentUrl() === 'update-admin/{id}')
                <a href="#" class="active">
            @else
                <a href="#">
            @endif    
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                    <span>Admin Management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                @if(getCurrentUrl() === 'new-admin')
                    <li class="active"><a href="{{url('/new-admin')}}"><i class="fa fa-circle-o"></i> Add Admin</a></li>
                @else
                    <li><a href="{{url('/new-admin')}}"><i class="fa fa-circle-o"></i> Add Admin</a></li>
                @endif
                @if(getCurrentUrl() === 'view-admin')
                    <li class="active"><a href="{{url('/view-admin')}}"><i class="fa fa-circle-o"></i> View Admin</a></li>
                @else
                    <li><a href="{{url('/view-admin')}}"><i class="fa fa-circle-o"></i> View Admin</a></li>
                @endif
                </ul>
            </li>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-sitemap"></i>
                    <span>Categories</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{url('/new-categories')}}"><i class="fa fa-circle-o"></i> Add Categorie</a></li>
                    <li><a href="{{url('/view-categories')}}"><i class="fa fa-circle-o"></i> View Categorie</a></li>
                </ul>
            </li>
        </ul>
        <br />
    </section>
    <!-- /.sidebar -->
</aside>